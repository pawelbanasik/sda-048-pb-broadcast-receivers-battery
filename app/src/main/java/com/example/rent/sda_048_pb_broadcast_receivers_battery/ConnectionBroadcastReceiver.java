package com.example.rent.sda_048_pb_broadcast_receivers_battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by RENT on 2017-06-14.
 */

public class ConnectionBroadcastReceiver extends BroadcastReceiver{

    ConnectionListener listener;

    public ConnectionBroadcastReceiver(ConnectionListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // logika
        listener.onIntentReceived();
    }

    interface ConnectionListener{
        void onIntentReceived();
    }

}
