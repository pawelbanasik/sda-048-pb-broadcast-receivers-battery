package com.example.rent.sda_048_pb_broadcast_receivers_battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by RENT on 2017-06-14.
 */

public class BatteryBroadcastReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals(Intent.ACTION_BATTERY_LOW)){
            Toast.makeText(context, "Battery is low!", Toast.LENGTH_SHORT).show();
        } else if (intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED)){
            Toast.makeText(context, "Power disconnected!", Toast.LENGTH_SHORT).show();
        }

    }


}
