package com.example.rent.sda_048_pb_broadcast_receivers_battery;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements ConnectionBroadcastReceiver.ConnectionListener {

    // zadanie 1 i 2 w jednym
    // zadanie 1 jest skonczone
    // zadanie 2 jest nieskonczone
    // zadanie 3 jest juz w innym pliku

    private Button buttonConnection;
    private ConnectionBroadcastReceiver connectionBroadcastReceiver;
    private IntentFilter intentFilter = new IntentFilter("key");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonConnection = (Button) findViewById(R.id.button_connection);
    }


    @Override
    public void onIntentReceived() {

    }
}
